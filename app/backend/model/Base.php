<?php

namespace app\backend\model;


/**
 *
 * 模型基础类
 *
 * Class Base
 * Author: zsw zswemail@qq.com
 */
class Base extends \think\Model{

    protected $autoWriteTimestamp = 'int';

    protected $dateFormat = 'Y-m-d H:i:s';

    public static $search = [];

    /**
     * 单字段修改
     *
     * @param $field
     * @param $value
     * @param $pk
     *
     * @return mixed
     * @throws \think\db\concern\DbException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function editField($field, $value, $pk)
    {
        return $this->findOrFail($pk)->save([$field=>$value]);
    }


}
